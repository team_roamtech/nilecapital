package com.roamtech.android.nilecapital.activities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.roamtech.android.nilecapital.R;
import com.roamtech.android.nilecapital.fragments.LoginStateFragment;
import com.roamtech.android.nilecapital.utils.CommonUtils;

public class LoginActivity extends ActionBarActivity{
    public static final String TAG = "LoginActivity";
    LoginStateFragment loginStateFragment;
    CommonUtils commonUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        commonUtils = new CommonUtils(this);
        loginStateFragment = new LoginStateFragment();
        commonUtils.addFragment(loginStateFragment,false, FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(), ((Object) loginStateFragment).getClass().getName());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
