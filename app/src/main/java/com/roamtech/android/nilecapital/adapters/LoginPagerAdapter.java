package com.roamtech.android.nilecapital.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.roamtech.android.nilecapital.R;
import com.roamtech.android.nilecapital.fragments.AdminLoginFragment;
import com.roamtech.android.nilecapital.fragments.LoginFragment;
import com.roamtech.android.nilecapital.fragments.RegistrationFragment;

/**
 * Created by Karuri on 4/26/2015.
 */
public class LoginPagerAdapter extends FragmentStatePagerAdapter {
    Context ctx;
    LoginFragment loginFragment;
    AdminLoginFragment adminLoginFragment;
    RegistrationFragment registrationFragment;


    public LoginPagerAdapter(Context ctx,FragmentManager fm){
        super(fm);
        this.ctx = ctx;

    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                loginFragment = new LoginFragment();
                return loginFragment;
            case 1:
                adminLoginFragment = new AdminLoginFragment();
                return adminLoginFragment;
            case 2:
                registrationFragment = new RegistrationFragment();
                return registrationFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}


