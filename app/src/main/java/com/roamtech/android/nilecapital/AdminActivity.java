package com.roamtech.android.nilecapital;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.roamtech.android.nilecapital.R;
import com.roamtech.android.nilecapital.fragments.AdminAccountFragment;
import com.roamtech.android.nilecapital.fragments.UserAccountFragment;
import com.roamtech.android.nilecapital.utils.CommonUtils;

public class AdminActivity extends ActionBarActivity {
    CommonUtils commonUtils;
    AdminAccountFragment adminAccountFragment;
    Toolbar toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        commonUtils = new CommonUtils(this);
        adminAccountFragment = new AdminAccountFragment();
        toolBar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        commonUtils.addFragment(adminAccountFragment,false, FragmentTransaction.TRANSIT_NONE,R.id.lyt_main_menu,getSupportFragmentManager(),((Object) adminAccountFragment).getClass().getName());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
