package com.roamtech.android.nilecapital.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Karuri on 4/26/2015.
 */
public class CommonUtils {
    Context ctx;
    public CommonUtils(Context ctx){
        this.ctx = ctx;
    }

    public void addFragment(Fragment fragment, boolean addToBackStack, int transition,int layoutResourceID, FragmentManager fm, String tag){
        //Whatever
        boolean fragmentPopped = fm.popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (!fragmentPopped && fm.findFragmentByTag(tag) == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(layoutResourceID, fragment);
            ft.setTransition(transition);
            if (addToBackStack)
                ft.addToBackStack(tag);
            ft.commit();
        }
    }

    public FragmentTransaction addDialogFragment(FragmentManager fm){
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        //ft.addToBackStack(null);
        return ft;
    }

}
