package com.roamtech.android.nilecapital.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roamtech.android.nilecapital.R;

/**
 * Created by Karuri on 10/1/2014.
 */
public class MyAccountAdapter extends BaseAdapter {
    public static final String TAG = "MyAccountAdapter";
    private Context context;
    private String[] mTitle;
    private int[] mIcon;
    private LayoutInflater inflater;
    private int layoutResourceID;

    public MyAccountAdapter(Context context, int layoutResourceID, String[] title, int[] icon) {
        this.context = context;
        this.layoutResourceID = layoutResourceID;
        this.mTitle = title;
        this.mIcon = icon;
    }

    @Override
    public int getCount() {
        return mTitle.length;
    }

    @Override
    public Object getItem(int position) {
        return mTitle[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtTitle;
        ImageView imgIcon;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(layoutResourceID, parent,
                false);

        // Locate the TextViews in drawer_list_item.xml
        txtTitle = (TextView) itemView.findViewById(R.id.item_text);

        // Locate the ImageView in drawer_list_item.xml
        imgIcon = (ImageView) itemView.findViewById(R.id.item_image);

        txtTitle.setText(mTitle[position]);

        // Set the results into ImageView
        imgIcon.setImageResource(mIcon[position]);
        return itemView;
    }
}
