package com.roamtech.android.nilecapital.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.roamtech.android.nilecapital.R;
import com.roamtech.android.nilecapital.UserMainActivity;
import com.roamtech.android.nilecapital.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.nilecapital.utils.CommonUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{
    public static final String TAG = "LoginFragment";
    ActionBarActivity mActivity;
    CommonUtils commonUtils;
    TextView txtTitle,txtForgotPassword;
    ProgressDialogFragment progressDialogFragment;
    EditText editPhoneNum,editPassword;
    Button btnLogin;
    CheckBox chkShowPassword,chkRememberMe;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActionBarActivity)activity;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        progressDialogFragment = new ProgressDialogFragment();
        commonUtils = new CommonUtils(mActivity);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        editPhoneNum = (EditText) rootView.findViewById(R.id.edit_phone_num);
        editPassword = (EditText) rootView.findViewById(R.id.edit_password);
        btnLogin = (Button) rootView.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);
        txtTitle = (TextView) rootView.findViewById(R.id.txt_title);
        txtForgotPassword = (TextView) rootView.findViewById(R.id.txt_forgot_password);
        txtForgotPassword.setText(Html.fromHtml(getResources().getString(R.string.txt_forgot_password)));
        chkShowPassword = (CheckBox) rootView.findViewById(R.id.chk_show_password);
        chkRememberMe = (CheckBox) rootView.findViewById(R.id.chk_remember_me);
        chkShowPassword.setOnCheckedChangeListener(this);
        chkRememberMe.setOnCheckedChangeListener(this);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        Intent i = new Intent(mActivity, UserMainActivity.class);
        startActivity(i);
        mActivity.finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(compoundButton == chkShowPassword) {
            if (isChecked) {
                //show password
                Log.d(TAG, " -------------- show password" + isChecked);
                editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                //hide password
                Log.d(TAG, " --------------  hide password" + isChecked);
                editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
        else if(compoundButton == chkRememberMe) {

        }
    }
}
