package com.roamtech.android.nilecapital.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.roamtech.android.nilecapital.R;
import com.roamtech.android.nilecapital.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.nilecapital.utils.CommonUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{
    public static final String TAG = "RegistrationFragment";
    ActionBarActivity mActivity;
    CommonUtils commonUtils;
    EditText editEmail,editMobileNum,editPassword,editConPassword;
    Button btnRegister;
    CheckBox chkShowPassword;
    ProgressDialogFragment progressDialogFragment;


    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActionBarActivity)activity;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        progressDialogFragment = new ProgressDialogFragment();
        commonUtils = new CommonUtils(mActivity);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);
        editEmail = (EditText) rootView.findViewById(R.id.edit_email);
        editMobileNum = (EditText) rootView.findViewById(R.id.edit_phone_num);
        editPassword = (EditText) rootView.findViewById(R.id.edit_password);
        editConPassword = (EditText) rootView.findViewById(R.id.edit_con_password);
        btnRegister = (Button) rootView.findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);
        chkShowPassword = (CheckBox) rootView.findViewById(R.id.chk_show_password);
        chkShowPassword.setOnCheckedChangeListener(this);
        return rootView;
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(compoundButton == chkShowPassword) {
            if (isChecked) {
                //show password
                Log.d(TAG, " -------------- show password" + isChecked);
                editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                editConPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

            } else {
                //hide password
                Log.d(TAG, " --------------  hide password" + isChecked);
                editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                editConPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }

    }

    @Override
    public void onClick(View v) {

    }
}
