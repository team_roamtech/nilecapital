package com.roamtech.android.nilecapital.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.roamtech.android.nilecapital.R;
import com.roamtech.android.nilecapital.adapters.MyAccountAdapter;
import com.roamtech.android.nilecapital.utils.AsyncLoader;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPolicyFragment extends Fragment implements LoaderManager.LoaderCallbacks<int[]>{
    public static final String TAG = "MyPolicyFragment";
    ListView lstMyAccount;
    ProgressBar mProgressBar;
    String [] accountItemsArray = {};
    MyAccountAdapter mMyAccountAdapter;
    ActionBarActivity mActivity;
    int [] icon;
    public static final int LOADER = 1003;


    public MyPolicyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ActionBarActivity) activity;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_account, container, false);
        lstMyAccount = (ListView) rootView.findViewById(R.id.lst_life_details);
        lstMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch(position){
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:


                        break;
                    default:
                        break;
                }
            }
        });
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        accountItemsArray = getResources().getStringArray(R.array.my_policy_items);
        getLoaderManager().initLoader(LOADER, null, this);
        return rootView;
    }

    public static class GetAccountItems extends AsyncLoader<int[]> {

        Context ctx;
        int[] icon;

        public GetAccountItems(Context context) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            Log.d(TAG, "It gets here");

        }

        @Override
        public int[] loadInBackground() {
            // TODO Auto-generated method stub
            icon = new int[]{R.drawable.ic_action_next_item, R.drawable.ic_action_next_item, R.drawable.ic_action_next_item,
                    R.drawable.ic_action_next_item, R.drawable.ic_action_next_item, R.drawable.ic_action_next_item,
                    R.drawable.ic_action_next_item, R.drawable.ic_action_next_item, R.drawable.ic_action_next_item,
                    R.drawable.ic_action_next_item, R.drawable.ic_action_next_item};

            return icon;
        }
    }


    @Override
    public Loader<int[]> onCreateLoader(int id, Bundle args) {
        mProgressBar.setVisibility(View.VISIBLE);
        return new GetAccountItems(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<int[]> loader, int[] data) {
        icon = data;
        Log.d(TAG, "OnLoadFinished");
        mMyAccountAdapter = new MyAccountAdapter(getActivity(),R.layout.my_account_items,accountItemsArray, icon);
        lstMyAccount.setAdapter(mMyAccountAdapter);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<int[]> loader) {

    }
}
